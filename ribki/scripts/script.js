// ГЛОБАЛЬНЫЕ ПАРАМЕТРЫ
/////////////////////
// Счет
var player = 'unnamed';
var score = 0;
// Попыток
var attempt = 10;
// Селектор отображения очков
var scoreDisplay = '#scoreDisplay';
// Количество пузерей
var numberOfBubbles = 30;
// Скорость всплывания
var bubbleSpeedMin = 2000;
var bubbleSpeedMax = 4500;
// Размер пузырей
var bubbleSizeMin = 10;
var bubbleSizeMax = 30;
// Количество рыб
var numberOfFishes = 15;
// Скорость рыб
var fishSpeedMin = 4000;
var fishSpeedMax = 8000;
// Размер Рыб
var fishSizeMin = 50;
var fishSizeMax = 150;
// Скорость исчезновения
var rateOfDisappearance = 100;
// ОКНО НАСТРОЕК
// Ширина окна
var settingWindowWidth = 350;
// Высота окна
var settingWindowHeight = 420;
// КНОПКА СТАРТА
// Ширина
var startButtonWidth = 400;
// Высота
var startButtonHeight = 60;
/////////////////////////////////////////////
// Картинки рыб
var fishPictures = [];
for (let i = 0; i <= 7; ++i)
    fishPictures.push('images/fishes/fish' + i + '.png');
// Количество картинок
var numberOfDifferentFishes = fishPictures.length

//MAIN
///////
$(function () {
    new Gameplay();
});

class Bubble {

    constructor(id) {
        this.id = id; // Номер id
        this.name = 'bubble' + id; // Полное имя
        this.selector = '#bubble' + id; // Селектор

        $('body').append(`<div id = "${this.name}"></div>`); // Создание div
        $(this.selector).addClass('bubbles');// Присвоить общий класс
    }

    setupBubble() // Разместить
    {
        this.speed = this.getRandSpeed(); // Получить случайную скорость
        this.size = this.getRandSize(); // Получить случайный размер
        this.posX = this.getRandPlaceX(); // Получить случайное место x
        this.posY = this.getRandPlaceY(); // Получить случайное место y
        this.d = getRect(this.size, window.innerWidth, window.innerHeight); // Получить перечислитель направлений
        this.direction = this.d.Top; // Получить направление

        $(this.selector).css({ 'width': this.size + 'px', 'height': this.size + 'px' }); // Установить размер
        $(this.selector).css({
            'border-radius': this.size + 'px',
            '-moz-border-radius': this.size + 'px',
            '-webkit-border-radius': this.size + 'px'
        }); // Скруглить
        $(this.selector).css({ 'left': this.posX, 'top': this.posY }); // Разместить
    }

    startAnimateBubble() // Запустить анимацию
    {
        this.action = true; // Анимация включена
        this.animateBubble(); // Вызов анимации
    }

    stopAnimateBubble() // Остановить анимацию
    {
        this.action = false; // Анимация остановлена
        $(this.selector).stop(); // Прервать анимацию
    }

    animateBubble() // Анимировать
    {
        //Появление
        $(this.selector).fadeIn(1);
        //Анимирование
        $(this.selector).animate(this.direction, this.speed);
        //Исчезновение
        $(this.selector).fadeOut(1, /* Вызов после исчезновения*/() => {
            // Если анимация включена
            if (this.action === true) {
                // Изменить пузырь
                this.setupBubble();
                // Рекурсивный вызов анимации
                this.animateBubble();
            }
        });
    }

    getRandSize() { return rand(bubbleSizeMin, bubbleSizeMax); } // Рандомный размер
    getRandPlaceX() { return rand(0, window.innerWidth - this.size); } // Рандомное расположение по x
    getRandPlaceY() { return rand(0, window.innerHeight - this.size); } // Рандомное расположение по y
    getRandSpeed() { return rand(bubbleSpeedMin, bubbleSpeedMax); } // Рандомная скорость
} // Class Bubble
// FISH
/////////
class Fish {

    constructor(id) {
        this.id = id; // Номер id
        this.name = 'fish' + id; // Полное имя
        this.selector = '#fish' + id; // Селектор

        $('body').append(`<div id ="${this.name}"></div>`); // Создание div
        $(this.selector).addClass('fishes'); // Присвоить общий класс

        this.activateHittingFish(); // Активировать событие при нажатие
    }

    setupFish() // Разместить
    {
        this.speed = this.getRandSpeed(); // Получить случайную скорость
        this.size = this.getRandSize(); // Получить случайный размер
        this.posY = this.getRandPlaceY(); // Получить случайное место по y
        this.posX = window.innerWidth - this.size; // Получить место по x
        this.index = this.getRandPicture(); // Получить случайное изображение
        this.aquaWidth = window.innerWidth - this.size; //Длина для перемещения
        this.aquaHeight = window.innerHeight - this.size; //Высота для перемещения
        this.d = getRect(this.size, window.innerWidth, window.innerHeight); // Получить перечислитель направлений
        this.direction = this.d.Left; // Начальное направление

        this.turnFish(h.Left, v.Up); // Повернуть рыбу

        $(this.selector).css({ 'background-image': 'url(' + fishPictures[this.index] + ')' }); // Установить картинку
        $(this.selector).css({ 'width': this.size + 'px', 'height': this.size + 'px' }); // Установить размер
        $(this.selector).css({ 'left': this.posX, 'top': this.posY }); // Установить расположение
    }

    updateFish() // Обновить
    {
        this.speed = this.getRandSpeed(); // Смена скорости
        // Смотрим прошлое направление
        switch (this.direction) {
            case this.d.Right: // Если был повернут вправо
                this.direction = this.d.Left; // Меняем направление влево
                this.turnFish(h.Left); // Поворот картинки вправо
                break;
            case this.d.Left: // Если был повернут влево
                this.direction = this.d.Right; // Меняем направление вправо
                this.turnFish(h.Right); // Поворот картинки влево
                break;
        }
    }

    startAnimateFish() // Запустить анимацию
    {
        this.action = true; // Анимация включена
        this.animateFish(); // Запустить анимацию
    }
    stopAnimateFish() // Остановить анимацию
    {
        this.action = false; // Анимация остановлена
        $(this.selector).stop() // Остановить анимацию
    }

    animateFish() // Анимировать
    {
        // Анимировать
        $(this.selector).animate(this.direction, this.speed, /* Вызов после завершения анимации */() => {
            // Если анимация запущена
            if (this.action === true) {
                this.updateFish(); // Обновить рыбу
                this.animateFish(); // Рекурсивный вызов
            }
        });
    }

    turnFish(horizontal, vertical = v.Up) // Повернуть рыбу
    {
        $(this.selector).css({
            '-moz-transform': 'scale(' + horizontal + ', ' + vertical + ')',
            '-webkit-transform': 'scale(' + horizontal + ', ' + vertical + ')',
            '-o-transform': 'scale(' + horizontal + ', ' + vertical + ')',
        });
    }

    activateHittingFish() // Событие при клике
    {
        $(this.selector).mousedown(() => {
            if (Gameplay.hasBegun === true) {
                $(scoreDisplay).html(`Очков: ${++score}`);
                $(this._attemptDisplay).html(`Попыток: ${++attempt}`);
                this.killFish();
            }
        });
    }

    killFish() // Убить рыбу и создать новую
    {
        $(this.selector).unbind('mousedown');
        $(this.selector).stop();
        $(this.selector).fadeOut(rateOfDisappearance);
        $(this.selector).fadeIn(rateOfDisappearance, () => {
            this.setupFish();
            this.startAnimateFish();
            this.activateHittingFish();
        });
    }

    getRandSize() { return rand(fishSizeMin, fishSizeMax); } // Рандомный размер
    getRandPlaceX() { return rand(0, window.innerWidth - this.size); } // Рандомное расположение по x
    getRandPlaceY() { return rand(0, window.innerHeight - this.size); } // Рандомное расположение по y
    getRandSpeed() { return rand(fishSpeedMin, fishSpeedMax); }  // Рандомная скорость
    getRandPicture() { return rand(0, numberOfDifferentFishes - 1); } // Рандомное изображение

} // Class Fish

//GAMEPLAY
/////////////
class Gameplay {
    constructor() {
        // Сменить курсор мышки
        document.body.style.cursor = 'url("images/cursors/cross.cur"), pointer';
        $('button').css('cursor', 'url("images/cursors/cross.cur"), pointer');
        // Создание пузырей
        createBubbles();
        // Создание рыб
        createFishes();

        this.styleDisplayBefore = { 'margin': 'auto auto', 'font-size': '36pt', 'width': '540px', 'height': '70px' };
        this.styleDisplayAfter = { 'margin': '0 0 auto auto', 'font-size': '26pt', 'width': '250px', 'height': '50px' };
        this._startButton = '#startButton'; // Селектор кнопки старт
        this._attemptDisplay = '#attemptDisplay'; // Селектор отображения попыток
        this._displayClass = '.gameplayDisplay'; // Общий класс вывода очков
        this._displayBtn = '.gameplayButton';

        $('body').append(`
		<div class="playersTable">
		<div id = "playersArea"></div>
		</div>`);
        $('body').append(`<div class="row">
                            <div id = "attemptDisplay" class = "col-md-3 gameplayDisplay" >
                            </div>
                        </div>`);
        $('body').append(`<div class="row">
                            <div id = "scoreDisplay" class ="col-md-3 gameplayDisplay">
                            </div>
                        </div>`);

        $('body').append(`<div class = "gameplayButton">
                        <form name="userForm">
                            <div >
                                <label for="name"></label>
                                <input type="text" class ="name" name="name" placeholder="Введите имя" />
                            </div>
                        </form>
                        <button id = "startButton" class = "btn btn-primary">
                            Старт
                        </button>
                    </div>`);
        Gameplay.hasBegun = false;
        onGetPlayers();
        this.prepareGame();
    }
    prepareGame() {
        $(this._displayClass).css(this.styleDisplayBefore);
        $(this._startButton).css({ 'width': `${startButtonWidth}px`, 'height': `${startButtonHeight}px` });
        $(this._startButton).css({ 'top': `${window.innerHeight - startButtonHeight}px`, 'left': `${window.innerWidth / 2 - startButtonWidth / 2}px` });
        $(this._displayClass).addClass('unselectable'); // Убрать выделение

        $(this._attemptDisplay).html(`Попыток: ${attempt}`);
        $(scoreDisplay).html(`Очков: ${score}`);

        $(this._startButton).click(() => {
            $(scoreDisplay).html(`Очков: ${score = 0}`);
            $(this._attemptDisplay).html(`Попыток: ${attempt = 10}`);
            player = $('.name').val();
            $(this._displayBtn).fadeOut(500);
            $(this._displayClass).animate(this.styleDisplayAfter, 500);
            this.startGame();
        });
    }

    startGame() {  // Установить событие окончания игры
        Gameplay.hasBegun = true;
        $('body').mousedown(() => {
            $(this._attemptDisplay).html(`Попыток: ${--attempt}`);
            if (attempt === 0) {
                onAddPlayer();
                this.endGame();
            }
        });
    }
    endGame() {
        Gameplay.hasBegun = false;
        $(this._displayBtn).fadeIn(500);
        $(this._displayClass).animate(this.styleDisplayBefore, 500);
        $('body').unbind('mousedown');
    }
}

function createBubbles() // Создание пузырей
{
    let bubbles = new Array();
    let intervalBubble = setInterval(function () {
        let i = bubbles.length;
        bubbles.push(new Bubble(i));
        bubbles[i].setupBubble();
        bubbles[i].startAnimateBubble();

        if (i === numberOfBubbles) {
            clearInterval(intervalBubble);
        }
    }, rand(200, 1500));
}

function createFishes() // Создание рыб
{
    let fishes = new Array();
    let intervalFish = setInterval(function () {
        let i = fishes.length;
        fishes.push(new Fish(i));
        fishes[i].setupFish();
        fishes[i].startAnimateFish();

        if (i === numberOfFishes) {
            clearInterval(intervalFish);
        }
    }, rand(660, 2000));
}
/////////////////////////////////////////////
// Вернуть рандом
function rand(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
// Получить перечислитель направлений
function getRect(size, wWidth, wHeight) {
    return {
        Left: { 'left': '0px' },
        Right: { 'left': (wWidth - size) + 'px' },
        Top: { 'top': '0px' },
        Bottom: { 'top': (wHeight - size) + 'px' }
    }
};
// Получить перечислитель для размещения картинки по вертикали
var v = { Up: 1, Down: -1 };
// Получить перечислитель для размещения картинки по горизонтали
var h = { Left: 1, Right: -1 };

async function onGetPlayers() {
    let res = await fetch(`http://localhost:3000/game`);
    let data = await res.json();
    $('#playersArea').empty();
    for (let i = 0; i < data.length; i++) {
        $('#playersArea').append(`
		<div>
			<div class = "myTable">
				id: ${data[i].id}, 
				имя: ${data[i].name}, 
				очков: ${data[i].score}
			</div>
		</div>`);
    }
}

async function onAddPlayer() {
    let res = await fetch(`http://localhost:3000/game`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: player,
                score: score
            })
        });
    /* 	const content = await res.json();
        console.log(content); */
    onGetPlayers();
}